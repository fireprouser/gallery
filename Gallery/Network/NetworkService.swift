import Foundation

class NetworkService {
    static let shared = NetworkService(service: AuthService.shared, queue: DispatchQueue.global(qos: .userInitiated))

    private let authService: AuthService
    private let queue: DispatchQueue

    private init(service: AuthService, queue: DispatchQueue) {
        self.queue = queue
        self.authService = service
    }

    func load<Result>(resourse: Resource<Result>, complition: @escaping (Result?, NetworkError?)->())  {

        self.authService.authorizeRequest(resource: resourse) { authrizedResource in

            guard let authResource = authrizedResource else {
                return
            }

            URLSession.shared.load(resourse: authResource) { [weak self] result, error in

                if let error = error {

                    switch error {
                    case .unauthorized:
                        self?.authService.resetToken()
                        self?.authService.authorizeRequest(resource: authResource, complition: { (reauthrizedResource) in

                            guard let reauthrizedResource = reauthrizedResource else {
                                return
                            }

                            self?.load(resourse: reauthrizedResource, complition: complition)
                        })
                    }
                    complition(nil, error)

                }

                complition(result, nil)

            }
        }
    }

}
