import Foundation

enum NetworkError: Error {
    case unauthorized
}

enum HTTPMethod<Body> {
    case get
    case post(Body)
}

extension HTTPMethod {
    var methodString: String {
        switch self {
        case .get: return "GET"
        case .post: return "POST"
        }
    }
}

struct Resource<A> {
    var request: URLRequest
    var parse: (Data) -> A?
}

extension Resource where A: Decodable {

    init(get url: URL) {
        self.request = URLRequest(url: url)

        self.parse = { data in
            try? JSONDecoder().decode(A.self, from: data)
        }
    }

    init<Body: Encodable>(url: URL, method: HTTPMethod<Body>) {
        self.request = URLRequest(url: url)

        self.request.httpMethod = method.methodString

        switch method {
        case .get: break
        case .post(let body):
            self.request.httpBody = try? JSONEncoder().encode(body)
            self.request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        }

        self.parse = { data in
            try? JSONDecoder().decode(A.self, from: data)
        }
    }
}

extension Resource {
    func map<B> (_ transform: @escaping (A) -> B) -> Resource<B> {
        return Resource<B>(request: self.request) { data in
            self.parse(data).map(transform)
        }
    }
}

extension URLSession {
    func load<Result>(resourse: Resource<Result>, complition: @escaping (Result?, NetworkError?)->()) {

        dataTask(with: resourse.request) {data, response, error in

            if let response = response as? HTTPURLResponse, let error = self.error(by: response) {
                complition(nil, error)
            }

            complition(data.flatMap { resourse.parse($0) }, nil)

        }.resume()
    }

    private func error(by response: HTTPURLResponse) -> NetworkError? {
        var error: NetworkError?

        switch response.statusCode {
        case 401:
            error = .unauthorized
        default:
            break
        }

        return error
    }
}
