import Foundation

//{
//    "auth": true,
//    "token": "0a023611c53890c74cfc1e816508d485e1fee174"
//}

struct Token: Codable {
    let token: String
}

class AuthService {
    struct Constants {
        static let authURL = URL(string: "http://195.39.233.28:8035/auth")!
        static let APICredentionals = ["apiKey": "23567b218376f79d9415"]
    }

    static let shared = AuthService()

    private(set) var token: Token?
    
    private let lock: NSLock
    private let queue: DispatchQueue

    private init() {
        self.queue = DispatchQueue(label: "AuthQueue")
        self.lock = NSLock()
    }

    private func renevalAuth(complition: @escaping (Token?, Error?) -> ()) {
        let resource = Resource<Token?>(url: Constants.authURL, method: .post(Constants.APICredentionals))
        
        URLSession.shared.load(resourse: resource) { token, error in
            complition(token?.flatMap({$0}), error)
        }
    }

    func resetToken() {
        self.token = nil
    }

    func authorizeRequest<A>(resource: Resource<A>, complition: @escaping (_ authorizedResource: Resource<A>?) -> ()){
        self.queue.async { [weak self] in

            guard let self = self else {
                return
            }

            guard let token = self.token else {
                self.lock.lock()
                self.renevalAuth {[weak self] token, error in
                    self?.token = token
                    self?.lock.unlock()
                    self?.authorizeRequest(resource: resource, complition: complition)
                }
                return
            }

            var resource = resource
            resource.request.auth(with: token)
            complition(resource)
        }
    }
}

private extension URLRequest {
    mutating func auth(with token: Token) {
        setValue("Bearer \(token.token)", forHTTPHeaderField: "Authorization")
    }
}
