//
//  ViewController.swift
//  Gallery
//
//  Created by Dmytro on 5/5/19.
//  Copyright © 2019 LampIdeaSoftware. All rights reserved.
//

import UIKit

class ViewController: UICollectionViewController {
    var viewModel: ImagesViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.viewModel = ImagesViewModel() { index in
            DispatchQueue.main.async {
                self.collectionView.insertItems(at: index.map({IndexPath(row: $0, section: 0)}))
                self.collectionView.refreshControl?.endRefreshing()
            }
        }
        self.viewModel.nextPage()

//        let refreshControl = UIRefreshControl(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
//        self.collectionView.refreshControl = refreshControl
//        refreshControl.addTarget(self, action: #selector(fetchNewImages), for: .valueChanged)
//        self.collectionView.
    }

    @objc func fetchNewImages() {
        self.viewModel.nextPage()
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.viewModel.images.count
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as? ImageCellVIew else {
            return UICollectionViewCell()
        }

        self.viewModel.getImage(by: indexPath.row) { imageData in
            guard let imageData = imageData, let image = UIImage(data: imageData) else {
                return
            }

            DispatchQueue.main.async {
                cell.imageView.image = image
            }
        }

        return cell
    }

    override func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == self.viewModel.images.count - 1 {
            self.viewModel.nextPage()
        }
    }
}

extension ViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let itemSide = self.collectionView.frame.width / 2

        return CGSize(width: itemSide, height: itemSide)
    }
}

