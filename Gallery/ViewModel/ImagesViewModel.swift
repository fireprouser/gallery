import Foundation

class DownloadOperation: Operation {
    let imageURL: URL
    let complition: (URL, Data?) -> ()

    init(with imageURL: URL, complition: @escaping (URL, Data?) -> ()) {
        self.imageURL = imageURL
        self.complition = complition
    }

    override func main() {
        if self.isCancelled { return }

        let imageData = try? Data(contentsOf: self.imageURL)

        self.complition(self.imageURL, imageData)
    }
}

class ImagesViewModel {

    private var currentPage: Int
    private var fetchingInProgress: Bool
    private var updateImages: ([Int]) -> ()

    private(set) var images: [Image]
    private(set) var donwloadedImages: [URL: Data]
    private(set) var model: ImagesModel
    private(set) var downloadOperation: OperationQueue
    private(set) var lastFetchedGallery: Gallery?
    private var pendingOperations: [DownloadOperation]

    init (updateImages: @escaping ([Int]) -> ()) {
        self.images = []
        self.currentPage = 4
        self.fetchingInProgress = false
        self.model = ImagesModel()
        self.donwloadedImages = [:]
        self.updateImages = updateImages
        self.pendingOperations = []
        self.downloadOperation = OperationQueue()
        self.downloadOperation.maxConcurrentOperationCount = 1
    }

    @objc
    func nextPage() {
        guard !fetchingInProgress else {
            return
        }

        self.fetchingInProgress = true

        self.currentPage += 1

        self.model.fetchImages(page: Page(number: currentPage)) { [weak self] gallery, error in
            self?.fetchingInProgress = false

            guard let gallery = gallery, let self = self else {
                return
            }
            self.lastFetchedGallery = gallery
            let oldImages = self.images
            self.images.append(contentsOf: gallery.pictures)
            self.updateImages(Array(oldImages.count..<self.images.count))
        }
    }

    func getImage(by index: Int, downloadedData: @escaping (Data?) -> ()) {
        let image = self.images[index]

        guard !self.pendingOperations.contains(where: {$0.imageURL == image.imageURL}) else {
            print("try to pend")
            return
        }

        guard let imageData = self.donwloadedImages[image.imageURL] else {

            let downloadOperation = DownloadOperation(with: image.imageURL, complition: { (url, imageData) in

                guard let imageData = imageData else {
                    return
                }

                self.donwloadedImages[url] = imageData
                downloadedData(imageData)

                guard let index = self.pendingOperations.firstIndex(where: {$0.imageURL == url}) else {
                    return
                }

                self.pendingOperations.remove(at: index)
            })

            self.pendingOperations.append(downloadOperation)

            self.downloadOperation.addOperation(downloadOperation)

            return
        }

        downloadedData(imageData)
    }
}
