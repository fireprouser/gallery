import Foundation

struct Page {
    var number: Int
}

struct Image: Codable {
    enum CodingKeys: String, CodingKey {
        case id
        case imageURL = "cropped_picture"
    }

    let id: String
    let imageURL: URL
}

struct Gallery: Codable {
    let hasMore: Bool
    let pageCount: Int
    let page: Int
    let pictures: [Image]
}

class ImagesModel {

    func fetchImages(page: Page, complition: @escaping (Gallery?, NetworkError?)->()) {
        guard let URL = URL(string: "http://195.39.233.28:8035/images?page=\(page.number)") else {
            return
        }

        NetworkService.shared.load(resourse: Resource<Gallery>(get: URL)) { gallery, error in complition(gallery, error) }
    }
}
